# Lien pour la version en ligne

https://roule_ta_boule.surge.sh

# Lien des maquettes sur Figma

https://www.figma.com/file/a3DHrXkjFbEauOmoLOkMSl/Roule-Ta-Boule

# Installation du projet

- Cloner le projet
- Ouvrir le fichier 'index.html' dans votre navigateur pour visualiser
- Ce projet utilise HTML5 et CSS3, Bootstrap est lié via CDN, il n'y a aucune installation à faire
- Modifier le code avec votre éditeur de texte favori !
- Mise en ligne avec [surge.sh](https://surge.sh/)